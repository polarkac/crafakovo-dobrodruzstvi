package cz.bamboomachine.mariocrafak.gfx;

import java.awt.Canvas;
import java.awt.Dimension;
import java.awt.Graphics2D;
import java.awt.image.BufferStrategy;

import cz.bamboomachine.mariocrafak.InputHandler;
import cz.bamboomachine.mariocrafak.Music;
import cz.bamboomachine.mariocrafak.gui.MainMenu;
import cz.bamboomachine.mariocrafak.gui.Menu;
import cz.bamboomachine.mariocrafak.levels.Level;

public class Screen extends Canvas {

	private static final long serialVersionUID = 1L;
	public static int width;
	public static int height;
	private Level level;
	private int offsetX;
	private InputHandler input;
	//private Image image;
	private Menu menu;
	
	public Screen(int _width, int _height) {
		width = _width;
		height = _height;
		Dimension dim = new Dimension(width, height);
		setPreferredSize(dim);
		setMinimumSize(dim);
		setMaximumSize(dim);		
		menu = new MainMenu(this);
		input = new InputHandler(this);
		offsetX = 0;
	}
	
	public void render() {	
		BufferStrategy buf = getBufferStrategy();
		if(buf == null) {
			createBufferStrategy(3);
			//image = createImage(width, height);
			return;
		}
		
		Graphics2D imgG = (Graphics2D) buf.getDrawGraphics(); //(Graphics2D)image.getGraphics();
		//imgG.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
		
		if(menu != null) {
			menu.render(imgG);
			Music.SONG.stop();
			Music.MENU.playLoop();
		} else {
			level.render(imgG);
			Music.SONG.playLoop();
			Music.MENU.stop();
		}
		
		//buf.getDrawGraphics().drawImage(image, 0, 0, null);
		
		imgG.dispose();
		buf.show();
	}

	public void tick() {
		input.tick();
		if(input.escape.isClicked() || !this.isFocusOwner())
			setMenu(new MainMenu(this));
		
		if(menu != null) {
			menu.tick();
			return;
		}
			
		level.tick();
		int posX = level.getPlayer().getPosX();
		setOffset(posX - (width / 2));
	}
	
	public void setOffset(int x) {
		offsetX = x;
		if(offsetX < 0)
			offsetX = 0;
		
		if((offsetX + width) > level.getLevelSize())
			offsetX = level.getLevelSize() - width;
	}
	
	public int getOffset() {
		return offsetX;
	}
	
	public InputHandler getInput() {
		return input;
	}

	public void setMenu(Menu menu) {
		this.menu = menu;
	}

	public Menu getMenu() {
		return menu;
	}
	
	@Override
	public int getWidth() {
		return width;
	}
	
	@Override
	public int getHeight() {
		return height;
	}
	
	public void setLevel(Level lvl) {
		level = lvl;
	}
	
	public Level getLevel() {
		return level;
	}
}
