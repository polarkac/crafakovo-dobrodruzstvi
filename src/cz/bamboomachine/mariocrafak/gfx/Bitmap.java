package cz.bamboomachine.mariocrafak.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.IOException;

import javax.imageio.ImageIO;

import cz.bamboomachine.mariocrafak.MarioCrafak;

public class Bitmap {
	
	public static final Sprite CRAFAK = loadSprite("/m_crafak2.png", 25, 44);
	public static final Sprite CREEPER = loadSprite("/m_creeper.png", 23, 40);
	public static final Sprite PIRANHA = loadSprite("/m_piranha.png", 26, 20);
	
	public static final BufferedImage GRASS1 = loadBitmap("/m_grass1.png");
	public static final BufferedImage GRASS2 = loadBitmap("/m_grass2.png");
	public static final BufferedImage GRASS3 = loadBitmap("/m_grass3.png");
	public static final BufferedImage GRASS4 = loadBitmap("/m_grass4.png");
	public static final BufferedImage CLOUD1 = loadBitmap("/m_cloud1.png");
	public static final BufferedImage CLOUD2 = loadBitmap("/m_cloud2.png");
	public static final BufferedImage CLOUD3 = loadBitmap("/m_cloud3.png");
	public static final BufferedImage CLOUD4 = loadBitmap("/m_cloud4.png");
	public static final BufferedImage BOX = loadBitmap("/m_box.png");
	public static final BufferedImage CREEPERBOX = loadBitmap("/m_box1.png");
	public static final BufferedImage HEARTHBOX = loadBitmap("/m_box2.png");
	public static final BufferedImage BRICK = loadBitmap("/m_brick1.png");
	public static final BufferedImage DARKBRICK = loadBitmap("/m_brickwall.png");
	public static final BufferedImage LIGHTPLANK = loadBitmap("/m_plank.png");
	public static final BufferedImage DARKPLANK = loadBitmap("/m_plankwall.png");
	public static final BufferedImage ERROR = loadBitmap("");

	public static Sprite loadSprite(String string, int width, int height) {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(MarioCrafak.class.getResourceAsStream(string));
		} catch(IllegalArgumentException e) {
			temp = new BufferedImage(width, height, BufferedImage.TYPE_INT_ARGB);
			Graphics g = temp.getGraphics();
			g.setColor(Color.RED);
			g.fillRect(0, 0, width, height);
			g.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return new Sprite(temp, width, height);
	} 
	
	public static BufferedImage loadBitmap(String name) {
		BufferedImage temp = null;
		try {
			temp = ImageIO.read(MarioCrafak.class.getResourceAsStream(name));
		} catch(IllegalArgumentException e) {
			temp = new BufferedImage(100, 100, BufferedImage.TYPE_INT_ARGB);
			Graphics g = temp.getGraphics();
			g.setColor(Color.RED);
			g.fillRect(0, 0, 40, 40);
			g.dispose();
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		return temp;
	}
}
