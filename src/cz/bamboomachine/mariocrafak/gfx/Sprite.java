package cz.bamboomachine.mariocrafak.gfx;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.awt.image.RasterFormatException;

public class Sprite {
	
	private BufferedImage img;
	private int imageWidth;
	private int imageHeight;
	
	public Sprite(BufferedImage img, int imgWidth, int imgHeight) {
		this.img = img;
		imageWidth = imgWidth;
		imageHeight = imgHeight;
	}
	
	public BufferedImage getImage(int posX, int posY) {
		BufferedImage temp = null;
		try {
			temp = img.getSubimage(posX*imageWidth, posY*imageHeight, imageWidth, imageHeight);
		} catch(RasterFormatException e) {
			temp = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_ARGB);
			Graphics g = temp.getGraphics();
			g.setColor(Color.RED);
			g.fillRect(0, 0, imageWidth, imageHeight);
			g.dispose();
		}
		return temp;
	}

	public int getWidth() {
		return imageWidth;
	}
	
	public int getHeight() {
		return imageHeight;
	}
}
