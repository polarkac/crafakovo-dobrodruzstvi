package cz.bamboomachine.mariocrafak.entities;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import cz.bamboomachine.mariocrafak.InputHandler;
import cz.bamboomachine.mariocrafak.Sound;
import cz.bamboomachine.mariocrafak.blocks.Block;
import cz.bamboomachine.mariocrafak.entities.particles.BubbleTextParticle;
import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.levels.Level;

public class Player extends Entity {

	private float animTimer;
	private double jumpTimer;
	public static final int SPEED = 4;
	private int direction;
	private InputHandler input;
	private int prevJumpY;
	private int health = 100;
	private int hurtTime = 0;
	private float knockTime = 0;
	private boolean knockToLeft = false;
	
	public Player(Level lvl, int x, int y) {
		super(x, y, Bitmap.CRAFAK);
		direction = 1;
		level = lvl;
		input = lvl.getInput();
		animTimer = 0;
		jumpTimer = 0;
		prevJumpY = 0;
	}
	
	private void walk() {
		int dx = 0;
		if(input.left.isDown()) {
			dx = -SPEED;
			direction = 1;
		} else if(input.right.isDown()) {
			dx = SPEED;
			direction = 0;
		}
		
		if(dx != 0) {		
			animTimer += 0.1;
			Block bl;
			if((bl = level.isColliding(this, getPosX() + dx, getPosY())) instanceof Block) {
				animTimer = 0;
				if(direction == 0)
					setPos(bl.getPosX() - getSprite().getWidth() - 1, getPosY());
				else 
					setPos(bl.getPosX() + bl.getImage().getWidth(), getPosY());					
				return;
			} else
				setPos(getPosX() + dx, getPosY());
			if(animTimer > 3)
				animTimer = 0;
		} else
			animTimer = 0;
	}
	
	private void jump() {
		if(input.space.isDown() && prevJumpY == 0 && level.isColliding(this, getPosX(), getPosY() + 3) instanceof Block) {
			jumpTimer = 0.1;
			prevJumpY = getPosY();
			Sound.JUMP.play();
		}
		
		if(jumpTimer == 0 && prevJumpY == 0)
			return;
		
		int sin = (int)(Math.sin(jumpTimer) * 45);
		jumpTimer += 0.09;
		animTimer = 3;
		
		Block bl;
		if((bl = level.isColliding(this, getPosX(), prevJumpY - sin)) instanceof Block) {
			jumpTimer = 0;
			prevJumpY = 0;
			animTimer = 0;
			if(jumpTimer > Math.PI / 2)
				setPos(getPosX(), bl.getPosY() + bl.getImage().getHeight());
			
			return;
		} else
			setPos(getPosX(), prevJumpY - sin);
		
		if(jumpTimer >= Math.PI) {
			jumpTimer = 0;
			prevJumpY = 0;
			animTimer = 0;
		}
	}
	
	private void fall() {
		if(jumpTimer != 0 && prevJumpY != 0)
			return;
		
		int posX = getPosX();
		int posY = getPosY() + 4;
		setPos(posX, posY);
		
		Block block;
		if((block = level.isColliding(this, posX, posY)) instanceof Block) {
			int blPosY = block.getPosY();
			setPos(posX, blPosY - getSprite().getHeight());
		} else
			animTimer = 3;
	}
	
	public BufferedImage getImage() {
		BufferedImage temp = null;
		temp = getSprite().getImage(direction, (int)animTimer);
		return temp;
	}

	public void render(Graphics2D g) {
		g.drawString("Zdraví: " + health, 500, 20);
		if(hurtTime > 0) {
			Color cl = new Color(255, 0, 0, 85);
			g.setColor(cl);
			g.drawImage(getImage(), getPosX() - level.getScreen().getOffset(), getPosY(), null);
			g.fillRect(getPosX() - level.getScreen().getOffset(), getPosY(), getSprite().getWidth(), getSprite().getHeight());
			hurtTime--;
		} else {
			g.drawImage(getImage(), getPosX() - level.getScreen().getOffset(), getPosY(), null);
		}
			
		//g.drawRect(getPosX() - level.getScreen().getOffset(), getPosY(), getSprite().getWidth(), getSprite().getHeight());
	}
	
	@Override
	public void tick() {		
		walk();
		jump();
		fall();
		
		if(knockback < 0) {
			setPos(getPosX() - 2, getPosY() + knockback / 6);
			knockback += 2;
		} else if(knockback > 0) {
			setPos(getPosX() + 2,  getPosY() - knockback / 6);
			knockback -= 2;
		}
		
		if(getPosX() > level.getLevelSize())
			setPos(-getImage().getWidth() / 2, getPosY());
		if(getPosX() + getImage().getWidth() < 0)
			setPos(level.getLevelSize() - getImage().getWidth() / 3, getPosY());
		
		
		if(input.x.isClicked()) {
			level.addParticle(new BubbleTextParticle("Ahoj ahoj ahoj!", this));
			Sound.AHOJ.play();
		}
		else if(input.c.isClicked()) {
			level.addParticle(new BubbleTextParticle("Shoř a ochrň!", this));
			Sound.SHOR.play();
		}
	}
	
	public void hurt(Entity en, int dmg) {
		health -= dmg;
		hurtTime = 60;
		if(en.getPosX() > getPosX()) 
			knockback -= 50;
		else
			knockback += 50;
	}
	
	public int getHealth() {
		return health;
	}
}
