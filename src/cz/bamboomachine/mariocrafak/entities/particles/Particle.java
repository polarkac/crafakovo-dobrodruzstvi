package cz.bamboomachine.mariocrafak.entities.particles;

import cz.bamboomachine.mariocrafak.entities.Entity;
import cz.bamboomachine.mariocrafak.gfx.Sprite;

public abstract class Particle extends Entity {
	
	public Particle(int x, int y, Sprite spr) {
		super(x, y, spr);
	}

	abstract public void tick();
}
