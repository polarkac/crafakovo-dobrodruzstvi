package cz.bamboomachine.mariocrafak.entities.particles;

import java.awt.Color;
import java.awt.FontMetrics;
import java.awt.Graphics2D;
import java.util.ArrayList;
import java.util.Collections;

import cz.bamboomachine.mariocrafak.entities.Entity;
import cz.bamboomachine.mariocrafak.gfx.Screen;
import cz.bamboomachine.mariocrafak.gfx.Sprite;

public class BubbleTextParticle extends Particle {

	private String[] msg;
	private Entity entity;
	private int timer = 0;
	private int biggestWidth = 0;
	
	public BubbleTextParticle(String message, Entity en) {
		super(en.getPosX(), en.getPosY(), null);
		
		msg = message.split("\n");
		entity = en;
	}
	
	@Override
	public Sprite getSprite() { return null; }

	@Override
	public void tick() {
		timer++;
		if(timer >= 60)
			isRemoved = true;
		
		setPos(entity.getPosX(), entity.getPosY());
	}
	
	@Override
	public void render(Graphics2D g, Screen scr) {
		FontMetrics metr = g.getFontMetrics();
		if(biggestWidth == 0) {
			ArrayList<Integer> widths = new ArrayList<Integer>();
			for(String txt : msg) {
				widths.add(metr.stringWidth(txt));
			}
			Collections.sort(widths);
			biggestWidth = widths.get(widths.size() - 1);
		}
		int bubbleHeight = msg.length * metr.getHeight() + 20;
		int bubbleWidth = biggestWidth + 10;
		int bubblePosX = getPosX() - bubbleWidth / 2 + 12;
		int bubblePosY = getPosY() - bubbleHeight - 25;
		int textPosX = bubblePosX + bubbleWidth / 2 - biggestWidth / 2;
		int textPosY = bubblePosY + metr.getHeight() + 5;
		
		g.setColor(Color.WHITE);
		g.fillRoundRect(bubblePosX - scr.getOffset(), bubblePosY, bubbleWidth, bubbleHeight, 10, 10);
		g.setColor(Color.BLACK);
		g.drawRoundRect(bubblePosX - scr.getOffset(), bubblePosY, bubbleWidth, bubbleHeight, 10, 10);
		g.setColor(Color.BLACK);
		drawString(g, this.msg, textPosX - scr.getOffset(), textPosY, metr.getHeight());
		
		g.setColor(Color.BLACK);
		g.drawOval(getPosX() + 25 - scr.getOffset(), getPosY() - 20, 10, 10);
		g.setColor(Color.WHITE);
		g.fillOval(getPosX() + 25 - scr.getOffset(), getPosY() - 20, 10, 10);
		g.setColor(Color.BLACK);
		g.drawOval(getPosX() + 15 - scr.getOffset(), getPosY() - 10, 5, 5);
		g.setColor(Color.WHITE);
		g.fillOval(getPosX() + 15 - scr.getOffset(), getPosY() - 10, 5, 5);
	}

	private void drawString(Graphics2D g, String[] text, int posX, int posY, int fontHeight) {
		int counter = 0;
		for(String txt : msg) {
			g.drawString(txt, posX, posY + (fontHeight * counter));
			counter++;
		}
	}
}
