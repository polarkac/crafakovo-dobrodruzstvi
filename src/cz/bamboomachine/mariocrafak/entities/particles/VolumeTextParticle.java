package cz.bamboomachine.mariocrafak.entities.particles;

import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.gfx.Screen;


public class VolumeTextParticle extends Particle {
	
	private int timer = 0;
	private String msg;

	public VolumeTextParticle(String message, int x, int y) {
		super(x, y, null);
		msg = message;
	}

	@Override
	public void tick() {
		timer++;
		if(timer >= 60) 
			isRemoved = true;
	}

	@Override
	public void render(Graphics2D g, Screen scr) {
		g.drawString(msg, getPosX(), getPosY());
	}
	
	

}
