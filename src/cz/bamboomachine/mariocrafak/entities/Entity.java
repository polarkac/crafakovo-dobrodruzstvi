package cz.bamboomachine.mariocrafak.entities;

import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.gfx.Screen;
import cz.bamboomachine.mariocrafak.gfx.Sprite;
import cz.bamboomachine.mariocrafak.levels.Level;

abstract public class Entity {
	
	private int posX;
	private int posY;
	private Sprite sprite;
	protected Level level;
	protected boolean isRemoved = false;
	protected int knockback = 0;
	
	public Entity(int x, int y, Sprite spr) {
		posX = x;
		posY = y;
		sprite = spr;
	}
	
	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public Sprite getSprite() {
		return sprite;
	}
	
	public boolean getIsRemoved() {
		return isRemoved;
	}
	
	public void setPos(int posX, int posY) {
		// kontral kolize nejprve v X a pak v Y pozici
		this.posX = posX;
		this.posY = posY;
	}
	
	public void render(Graphics2D g, Screen scr) {
		int posX = getPosX();
		int posY = getPosY();
		int offset = scr.getOffset();
		if(posX + getSprite().getWidth() < offset || posX > offset + Screen.width)
			return;
		
		g.drawImage(getSprite().getImage(0, 0), posX - offset, posY, null);
	}
	
	abstract public void tick(); 
}
