package cz.bamboomachine.mariocrafak.entities;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.levels.Level;

public class Piranha extends Enemy {

	public Piranha(Level lvl, int x, int y) {
		super(x, y, Bitmap.PIRANHA);
		level = lvl;
	}
}
