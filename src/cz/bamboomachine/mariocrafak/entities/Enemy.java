package cz.bamboomachine.mariocrafak.entities;

import java.awt.Point;

import cz.bamboomachine.mariocrafak.blocks.Block;
import cz.bamboomachine.mariocrafak.gfx.Sprite;

public class Enemy extends Entity {
	
	private int radius;
	private Point centerPoint = new Point();
	protected int direction;
	protected static final int SPEED = 2;
	
	public Enemy(int x, int y, Sprite spr) {
		super(x, y, spr);
		radius = 150;
		centerPoint.setLocation(x, y);
		direction = 1;
	}

	@Override
	public void tick() {
		walk();
		fall();
	}
	
	protected void walk() {
		int posX = getPosX();
		int posY = getPosY();
		
		if((posX + SPEED + getSprite().getWidth()) >= (centerPoint.x + radius)) {
			direction = 0;
		} else if((posX - SPEED) <= (centerPoint.x - radius)) {
			direction = 1;
		}
		
		Block colBlock;
		if((colBlock = level.isColliding(this, posX, posY)) instanceof Block) {
			int blockPosX = colBlock.getPosX();
			int blockWidth = colBlock.getImage().getWidth();
			int plWidth = getSprite().getWidth();
			
			if(direction == 0) {
				posX = blockPosX + blockWidth;
				direction = 1;
			} else {
				posX = blockPosX - plWidth;
				direction = 0;
			}
		}
		
		if(direction == 0)
			posX -= SPEED;
		else if(direction == 1) 
			posX += SPEED;
		
		setPos(posX, posY);
	}
	
	protected void fall() {
		Block colBlock;
		int posX = getPosX();
		int posY = getPosY() + 2;
		if(level.isColliding(this, posX, posY + 1) instanceof Block)
			return;
		
		if((colBlock = level.isColliding(this, posX, posY)) instanceof Block) {
			int blockPosY = colBlock.getPosY();
			int plHeight = getSprite().getHeight();
						posY = blockPosY + plHeight;
			
		}
		setPos(posX, posY);
	}

}
