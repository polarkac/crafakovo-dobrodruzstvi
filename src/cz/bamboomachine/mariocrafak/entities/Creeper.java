package cz.bamboomachine.mariocrafak.entities;

import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.blocks.Block;
import cz.bamboomachine.mariocrafak.entities.particles.BubbleTextParticle;
import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.gfx.Screen;
import cz.bamboomachine.mariocrafak.levels.Level;

public class Creeper extends Enemy {
	
	private int explodeTime = 0;

	public Creeper(Level lvl, int x, int y) {
		super(x, y, Bitmap.CREEPER);
		level = lvl;
	}
	
	@Override
	public void tick() {
		fall();
		walk();
		
		if(explodeTime > 0)
			explodeTime--;
	}
	
	@Override
	protected void walk() {
		Player pl = level.getPlayer();
		int plX = pl.getPosX();
		int creepX = getPosX();
		int creepY = getPosY();
		int nextCreepX = creepX;
		int vectorLength = plX - creepX;
		
		if(vectorLength < 0) {
			int length = Math.abs(vectorLength);
			if(length < 150) {
				nextCreepX -= SPEED;
				direction = 1;
			}
		} else {
			if(vectorLength < 150) {
				nextCreepX += SPEED;
				direction = 0;
			}
		}
		
		if(Math.abs(vectorLength) <= SPEED) {
			nextCreepX = plX;
			explode();
		}
		
		Block bl = null;
		if((bl = level.isColliding(this, nextCreepX, creepY)) instanceof Block) {
			return;
		}
		
		setPos(nextCreepX, creepY);
	}
	
	public void explode() {
		Player pl = level.getPlayer();
		int plX = pl.getPosX();
		int creepX = getPosX();
		int creepY = getPosY();
		int nextCreepX = creepX;
		int vectorLength = Math.abs(plX - creepX);
		
		if(vectorLength < 5 && explodeTime == 0) {
			level.addParticle(new BubbleTextParticle("BOOOM!", this));
			explodeTime = 30;
		}
	}
	
	@Override
	public void render(Graphics2D g, Screen scr) {
		int posX = getPosX();
		int posY = getPosY();
		int offset = scr.getOffset();
		int imgY = 0;
		if(posX + getSprite().getWidth() < offset || posX > offset + Screen.width)
			return;
		
		if(explodeTime >= 30) 
			imgY = 5;
		else if(explodeTime > 15 && explodeTime < 30)
			imgY = 6;
		else if(explodeTime > 0 && explodeTime < 15) {
			imgY = 7;	
			if(explodeTime >= 0 && explodeTime < 5 && isRemoved != true) {
				level.getPlayer().hurt(this, 30);
				isRemoved = true;
			}
		}
		
		g.drawImage(getSprite().getImage(0, imgY), posX - offset, posY, null);
	}
}
