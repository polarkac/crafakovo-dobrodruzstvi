package cz.bamboomachine.mariocrafak.gui;

import java.awt.Graphics2D;

public interface Menu {
	
	public void render(Graphics2D g);
	public void tick();
	
}
