package cz.bamboomachine.mariocrafak.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.gfx.Screen;

public class ControlsMenu implements Menu {
	
	private Screen screen;
	private Font font = new Font(Font.MONOSPACED, Font.PLAIN, 20);
	private String[] controls = {"Hra se ovládá pomocí šipek na klávesnici.", "",
			"Mezerník slouží ke skákání.", "",
			"X a C slouží pro různé akce (prozatím spouští jen hlášky).", "",
			"Klávesa F2 slouží k zesílení zvuku, klávesa F3 ke ztlumení.", "",
			"Klávesa F4 slouží k zesílení hudby, klávesa F5 ke ztlumení"};
	
	public ControlsMenu(Screen scr) {
		screen = scr;
	}
	
	@Override
	public void render(Graphics2D g) {
		int scrWidth = screen.getWidth();
		int scrheight = screen.getHeight();
		g.setFont(font);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, scrWidth, scrheight);
		
		for(int a = 0; a < controls.length; a++) {
			FontMetrics metr = g.getFontMetrics();
			g.setColor(Color.DARK_GRAY);
			g.drawString(controls[a], scrWidth / 2 - metr.stringWidth(controls[a]) / 2 + 1, metr.getHeight() * a + 75 + 1);
			g.setColor(Color.RED);
			g.drawString(controls[a], scrWidth / 2 - metr.stringWidth(controls[a]) / 2, metr.getHeight() * a + 75);
		}
	}

	@Override
	public void tick() {}
}
