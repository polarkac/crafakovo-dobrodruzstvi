package cz.bamboomachine.mariocrafak.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.gfx.Screen;

public class AboutMenu implements Menu {
	
	private Screen screen;
	private int timer;
	private Font font = new Font(Font.MONOSPACED, Font.PLAIN, 20);
	private String[] about = {"Hra je věnována skvělému Let\'s Playerovy", 
			"jménem Crafák za jeho úžasná videa.", 
			"Tato hra vznikla k jeho prvnímu roku na", "YouTube.", "", "" , "",
			"Programování: Polarkac",
			"Grafika: grEy", 
			"Zvuky a hudba: Tiromir", "", "",
			"Díky ostatním kteří přispěli nápady nebo pomohli jinou cestou.", "(PeldaCZ, Vazzi, IceManSS)" , "",
			"Použité hlášky jsou vystřižené z videií a autorská", 
			"práva na ně vlastní Crafák :).", 
			"http://www.youtube.com/user/minecrafak"};
	
	public AboutMenu(Screen scr) {
		screen = scr;
		timer = 570;
	}
	
	@Override
	public void render(Graphics2D g) {
		int scrWidth = screen.getWidth();
		int scrheight = screen.getHeight();
		g.setFont(font);
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, scrWidth, scrheight);
		
		for(int a = 0; a < about.length; a++) {
			FontMetrics metr = g.getFontMetrics();
			g.setColor(Color.DARK_GRAY);
			g.drawString(about[a], scrWidth / 2 - metr.stringWidth(about[a]) / 2 + 1, metr.getHeight() * a + 75 + 1 + timer);
			g.setColor(Color.RED);
			g.drawString(about[a], scrWidth / 2 - metr.stringWidth(about[a]) / 2, metr.getHeight() * a + 75 + timer);
		}
	}

	@Override
	public void tick() {
		timer--;
		if(timer < -580)
			screen.setMenu(new MainMenu(screen));
	}
}
