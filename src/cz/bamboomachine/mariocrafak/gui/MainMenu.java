package cz.bamboomachine.mariocrafak.gui;

import java.awt.Color;
import java.awt.Font;
import java.awt.FontMetrics;
import java.awt.Graphics2D;

import cz.bamboomachine.mariocrafak.InputHandler;
import cz.bamboomachine.mariocrafak.MarioCrafak;
import cz.bamboomachine.mariocrafak.Sound;
import cz.bamboomachine.mariocrafak.gfx.Screen;
import cz.bamboomachine.mariocrafak.levels.Level;

public class MainMenu implements Menu {
	
	private Screen screen;
	private int option;
	private String[] buttons = {"Nová hra", "O hře", "Ovládání", "Ukončit"};
	private Font font = new Font(Font.MONOSPACED, Font.PLAIN, 25);
	private Font title = new Font(Font.MONOSPACED, Font.PLAIN, 50);
	
	public MainMenu(Screen scr) {
		option = 0;
		screen = scr;
	}
	
	@Override
	public void tick() {
		InputHandler input = screen.getInput();
		if(input.down.isClicked()) {
			option++;
			Sound.BUTTON_LONG.play();
		}
		if(input.up.isClicked()) {
			option--;
			Sound.BUTTON_LONG.play();
		}
		
		int l = buttons.length;
		if(option < 0) option = l - 1;
		if(option == l) option = 0;
		
		if(input.enter.isClicked())
			doAction();
	}
	
	@Override
	public void render(Graphics2D g) {
		if(screen.getLevel() != null)
			buttons[0] = "Zpět do hry";
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, screen.getWidth(), screen.getHeight());
		
		int posX = screen.getWidth() / 2;
		g.setFont(title);
		FontMetrics metr = g.getFontMetrics();
		g.setColor(Color.GRAY);
		g.drawString(MarioCrafak.gameName.toUpperCase(), posX - metr.stringWidth(MarioCrafak.gameName) / 2 + 1, 75 + 1);
		g.drawString(MarioCrafak.version.toUpperCase(), posX - metr.stringWidth(MarioCrafak.version) / 2 + 1, 75 + 51);
		g.setColor(Color.BLUE);
		g.drawString(MarioCrafak.gameName.toUpperCase(), posX - metr.stringWidth(MarioCrafak.gameName) / 2, 75);
		g.drawString(MarioCrafak.version.toUpperCase(), posX - metr.stringWidth(MarioCrafak.version) / 2, 75 + 50);
		
		
		g.setFont(font);
		metr = g.getFontMetrics();
		for(int a = 0; a < buttons.length; a++) {
			if(option == a)
				g.setColor(Color.CYAN);
			else
				g.setColor(Color.BLUE);
			
			g.fillRect(posX - 250 / 2, 190 + 100 * a, 250, 35);
			g.setColor(Color.DARK_GRAY);
			g.drawString(buttons[a], posX - metr.stringWidth(buttons[a]) / 2 + 1, 190 + 100 * a + 25 + 1);
			g.setColor(Color.RED);
			g.drawString(buttons[a], posX - metr.stringWidth(buttons[a]) / 2, 190 + 100 * a + 25);
		}
	}
	
	private void doAction() {
		switch(option) {
		case 0:
			if(screen.getLevel() == null) screen.setLevel(new Level(screen, 2500)); screen.setMenu(null); break;
		case 1: 
			screen.setMenu(new AboutMenu(screen)); break;
		case 2:
			screen.setMenu(new ControlsMenu(screen)); break;
		case 3:
			System.exit(0);
		}
		
	}
}
