package cz.bamboomachine.mariocrafak.levels;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.util.ArrayList;
import java.util.Random;

import cz.bamboomachine.mariocrafak.InputHandler;
import cz.bamboomachine.mariocrafak.Music;
import cz.bamboomachine.mariocrafak.Sound;
import cz.bamboomachine.mariocrafak.blocks.Block;
import cz.bamboomachine.mariocrafak.blocks.BoxBlock;
import cz.bamboomachine.mariocrafak.blocks.BoxCreeperBlock;
import cz.bamboomachine.mariocrafak.blocks.BoxHearthBlock;
import cz.bamboomachine.mariocrafak.blocks.CloudBlock;
import cz.bamboomachine.mariocrafak.blocks.DarkBrickBlock;
import cz.bamboomachine.mariocrafak.blocks.DarkPlankBlock;
import cz.bamboomachine.mariocrafak.blocks.GrassBlock;
import cz.bamboomachine.mariocrafak.blocks.GrayBrickBlock;
import cz.bamboomachine.mariocrafak.blocks.LightPlankBlock;
import cz.bamboomachine.mariocrafak.entities.Creeper;
import cz.bamboomachine.mariocrafak.entities.Entity;
import cz.bamboomachine.mariocrafak.entities.Piranha;
import cz.bamboomachine.mariocrafak.entities.Player;
import cz.bamboomachine.mariocrafak.entities.particles.Particle;
import cz.bamboomachine.mariocrafak.entities.particles.VolumeTextParticle;
import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.gfx.Screen;

public class Level {
	
	private Player pl;
	private int size;
	private InputHandler handler;
	private Screen screen;
	private BufferedImage background;
	private ArrayList<Block> blocks;
	private ArrayList<Entity> entities;
	
	public Level(Screen scr, int size) {
		this.size = size;
		handler = scr.getInput();
		pl = new Player(this, 450, 480); // 528
		screen = scr;
		background = Bitmap.loadBitmap("/m_sky.png");
		blocks = new ArrayList<Block>();
		entities = new ArrayList<Entity>();
		
		int levelBlockSize = (int)Math.ceil(size / 40.0);
		for(int a = -2; a < levelBlockSize + 2; a++) {
			blocks.add(new GrassBlock(a*40, 575));
		}
		blocks.add(new GrassBlock(120, 550));
		blocks.add(new GrassBlock(500, 550));
		blocks.add(new GrassBlock(860, 550));
		blocks.add(new GrassBlock(this.size, 520));
		blocks.add(new GrassBlock(-40, 520));
		
		blocks.add(new BoxBlock(560, 532));
		blocks.add(new BoxHearthBlock(600, 532));
		blocks.add(new BoxCreeperBlock(640, 532));
		blocks.add(new GrayBrickBlock(400, 460));
		blocks.add(new DarkBrickBlock(720, 532));
		blocks.add(new LightPlankBlock(760, 532));
		blocks.add(new DarkPlankBlock(800, 532));
		
		entities.add(new Creeper(this, 300, 500));
		entities.add(new Piranha(this, 670, 500));
		
		for(int a = 0; a < 10; a++) {
			Random rnd = new Random();
			int posX = rnd.nextInt(screen.getWidth());
			int posY = rnd.nextInt(200);
			blocks.add(new CloudBlock(posX, posY));
		}
	}
	
	public Player getPlayer() {
		return pl;
	}

	public void tick() {
		pl.tick();
		
		for(Block bl : blocks) {
			bl.tick();
		}
		
		for(int a = 0; a < entities.size(); a++) {
			Entity en = entities.get(a);
			en.tick();
			
			if(en.getIsRemoved())
				entities.remove(a--);	
			if((handler.f2.isClicked() || handler.f3.isClicked() || handler.f4.isClicked() || handler.f5.isClicked()) && en instanceof VolumeTextParticle)
				entities.remove(a--);
		}
		
		if(handler.f2.isClicked()) {
			Sound.increaseVolume();
			entities.add(new VolumeTextParticle("Sound volume: "+ Sound.getVolume(), 20, 25));
		} else if(handler.f3.isClicked()) {
			Sound.decreaseVolume();
			entities.add(new VolumeTextParticle("Sound volume: "+ Sound.getVolume(), 20, 25));
		} else if(handler.f4.isClicked()) {
			Music.increaseVolume();
			entities.add(new VolumeTextParticle("Music volume: "+ Music.getVolume(), 20, 25));
		} else if(handler.f5.isClicked()) {
			Music.decreaseVolume();
			entities.add(new VolumeTextParticle("Music volume: "+ Music.getVolume(), 20, 25));
		}
		
	}
	
	public void render(Graphics2D g) {
		int offsetX = screen.getOffset();
		int width = screen.getWidth();
		int height = screen.getHeight();
		
		g.setColor(Color.BLACK);
		g.fillRect(0, 0, width, height);
		
		int backWidth = background.getWidth();
		int backPos1 = backWidth * ((offsetX / 5) / backWidth) - offsetX / 5;
		int backPos2 = backWidth * ((offsetX / 5 + backWidth) / backWidth) - offsetX / 5;
		g.drawImage(background, backPos1, 0, null);
		g.drawImage(background, backPos2, 0, null);
		
		for(Block bl : blocks) {
			bl.render(g, screen);
		}
		
		for(Entity en : entities) {
			en.render(g, screen);
		}
		
		pl.render(g);
	}
	
	public Screen getScreen() {
		return screen;
	}

	public int getLevelSize() {
		return size;
	}
	
	public BufferedImage getBackground() {
		return background;
	}
	
	public Block isColliding(Entity e, int x, int y) {
		Rectangle rect1 = new Rectangle(x, y, e.getSprite().getWidth(), e.getSprite().getHeight());
		for(Block bl : blocks) {
			if(!bl.isBlocking())
				continue;
			Rectangle rect2 = new Rectangle(bl.getPosX(), bl.getPosY(), bl.getImage().getWidth(), bl.getImage().getHeight());
			if(rect1.intersects(rect2))
				return bl.isBlocking() ? bl : null;
		}
		
		return null;
	}
	
	public InputHandler getInput() {
		return handler;
	}

	public void addParticle(Particle par) {
		entities.add(par);
	}
}
