package cz.bamboomachine.mariocrafak;

import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.util.ArrayList;

import cz.bamboomachine.mariocrafak.gfx.Screen;
import cz.bamboomachine.mariocrafak.gui.MainMenu;
import cz.bamboomachine.mariocrafak.levels.Level;

public class InputHandler implements KeyListener {
	
	public class Key {
		private int presses, absorb;
		private boolean down, clicked;
		
		public Key() {
			keys.add(this);
		}
		
		public void toggle(boolean isPressed) {
			if(isPressed != down)
				down = isPressed;
			
			if(isPressed)
				presses++;
		}
		
		public void tick() {
			if(absorb < presses) {
				absorb++;
				clicked = true;
			} else {
				clicked = false;
			}
		}
		
		public boolean isClicked() {
			return clicked;
		}
		
		public boolean isDown() {
			return down;
		}
		
		public void reset() {
			down = clicked = false;
			presses = absorb = 0;
		}
	}

	private ArrayList<Key> keys = new ArrayList<Key>();
	private Screen screen;
	
	public Key left = new Key();
	public Key right = new Key();
	public Key up = new Key();
	public Key down = new Key();
	public Key space = new Key();
	public Key enter = new Key();
	public Key escape = new Key();
	public Key x = new Key();
	public Key c = new Key();
	public Key f2 = new Key();
	public Key f3 = new Key();
	public Key f4 = new Key();
	public Key f5 = new Key();
	
	public InputHandler(Screen scr) {
		scr.addKeyListener(this);
		screen = scr;
	}
	
	public void tick() {
		for(Key k : keys) {
			k.tick();
		}
	}
	
	@Override
	public void keyPressed(KeyEvent e) {
		toggleKey(e, true);
	}

	@Override
	public void keyReleased(KeyEvent e) {
		toggleKey(e, false);
	}
	
	private void toggleKey(KeyEvent e, boolean b) {
		switch(e.getKeyCode()) {
		case KeyEvent.VK_LEFT: left.toggle(b); break;
		case KeyEvent.VK_RIGHT:	right.toggle(b); break;
		case KeyEvent.VK_UP: up.toggle(b); break;
		case KeyEvent.VK_DOWN: down.toggle(b); break;
		case KeyEvent.VK_SPACE: space.toggle(b); break;
		case KeyEvent.VK_ENTER: enter.toggle(b); break; 
		case KeyEvent.VK_ESCAPE: escape.toggle(b); break;
		case KeyEvent.VK_X: x.toggle(b); break;
		case KeyEvent.VK_C: c.toggle(b); break;
		case KeyEvent.VK_F2: f2.toggle(b); break;
		case KeyEvent.VK_F3: f3.toggle(b); break;
		case KeyEvent.VK_F4: f4.toggle(b); break;
		case KeyEvent.VK_F5: f5.toggle(b); break;
		case KeyEvent.VK_F1: screen.setLevel(new Level(screen, 2500)); screen.setMenu(null); break;
		}
		
		if(e.isAltDown()) {
			resetAll();
			screen.setMenu(new MainMenu(screen));
		}
	}

	public void resetAll() {
		for(Key k : keys) {
			k.reset();
		}
	}

	@Override
	public void keyTyped(KeyEvent e) {}
}
