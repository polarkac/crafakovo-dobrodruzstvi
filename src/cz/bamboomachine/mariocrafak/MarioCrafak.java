package cz.bamboomachine.mariocrafak;

import java.awt.BorderLayout;

import javax.swing.JFrame;

import cz.bamboomachine.mariocrafak.gfx.Screen;

public class MarioCrafak implements Runnable {
	
	private boolean isRunning = false;
	private Thread thread;
	public final static String gameName = "Crafákovo dobrodružství";
	public final static String version = "1.7 dev";
	private Screen screen;
	private static JFrame frame;

	public static void main(String[] args) {
		frame = new JFrame(gameName + " - " + version);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frame.setVisible(true);
		frame.setResizable(false);
		frame.setLayout(new BorderLayout());
		
		MarioCrafak game = new MarioCrafak();
		game.start();
	}

	public void start() {
		if(isRunning == false)
			isRunning = true;
		thread = new Thread(this);
		thread.start();
	}
	
	public void stop() {
		if(isRunning)
			isRunning = false;
	}

	@Override
	public void run() {
		init();
		
		long nanoTimer = System.nanoTime();
		long fpsTimer = System.currentTimeMillis();
		int frames = 0;
		int ticks = 0;
		final double nsPerTick = 1000000000.0 / 60.0;
		double unprocessedTicks = 0;
		
		while(isRunning) {
			long now = System.nanoTime();
			unprocessedTicks += (now - nanoTimer) / nsPerTick;
			nanoTimer = now;
			
			while(unprocessedTicks >= 1) {
				screen.tick();
				ticks++;
				unprocessedTicks--;
			}
			
			screen.render();
			frames++;
			
			if((System.currentTimeMillis() - fpsTimer) >= 1000) {
				System.out.println("FPS: " + frames + ", ticks: " + ticks);
				fpsTimer = System.currentTimeMillis();
				frames = 0;
				ticks = 0;
			}
		}
	}

	private void init() {
		screen = new Screen(800, 600);
		frame.add(screen, BorderLayout.CENTER);
		frame.pack();
		screen.requestFocus();
	}
}
