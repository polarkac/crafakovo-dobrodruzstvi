package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class DarkBrickBlock extends Block {

	public DarkBrickBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.DARKBRICK);
	}

	@Override
	public void tick() {}

}
