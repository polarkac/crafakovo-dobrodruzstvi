package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class BoxCreeperBlock extends Block {

	public BoxCreeperBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.CREEPERBOX);
	}

	@Override
	public void tick() {}

}
