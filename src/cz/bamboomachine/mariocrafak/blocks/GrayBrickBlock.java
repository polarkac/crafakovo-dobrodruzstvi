package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class GrayBrickBlock extends Block {

	public GrayBrickBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.BRICK);
	}

	@Override
	public void tick() {}

}
