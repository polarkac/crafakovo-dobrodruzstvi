package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class LightPlankBlock extends Block {

	public LightPlankBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.LIGHTPLANK);
	}

	@Override
	public void tick() {}

}
