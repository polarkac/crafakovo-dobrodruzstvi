package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class BoxBlock extends Block {

	public BoxBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.BOX);
	}

	@Override
	public void tick() {}
}
