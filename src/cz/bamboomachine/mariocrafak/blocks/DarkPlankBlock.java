package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class DarkPlankBlock extends Block {

	public DarkPlankBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.DARKPLANK);
	}

	@Override
	public void tick() {}

}
