package cz.bamboomachine.mariocrafak.blocks;

import java.awt.Graphics2D;
import java.util.Random;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.gfx.Screen;

public class CloudBlock extends Block {
	
	private static final int SPEED = 1; 

	public CloudBlock(int x, int y) {
		super(x, y, false);
		Random rnd = new Random();
		switch(rnd.nextInt(4)) {
		case 0:
			setImage(Bitmap.CLOUD1); break;
		case 1:
			setImage(Bitmap.CLOUD2); break;
		case 2:
			setImage(Bitmap.CLOUD3); break;
		case 3:
			setImage(Bitmap.CLOUD4); break;
		}
	}
	
	@Override
	public void tick() {
		int x = getPosX();
		int y = getPosY();
		int screenWidth = Screen.width;
		Random rnd = new Random();
		
		if(x + getImage().getWidth() < 0) {
			y = rnd.nextInt(200);
			x = rnd.nextInt(screenWidth + 50);
			while(x < screenWidth)
				x = rnd.nextInt(screenWidth + 50);
		} else
			x -= SPEED;
		
		setPos(x, y);	
	}

	@Override
	public void render(Graphics2D g, Screen scr) {
		g.drawImage(getImage(), getPosX(), getPosY(), null);
	}
	
	
}
