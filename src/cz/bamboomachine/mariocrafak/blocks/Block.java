package cz.bamboomachine.mariocrafak.blocks;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;
import cz.bamboomachine.mariocrafak.gfx.Screen;

abstract public class Block {
	
	private int posX;
	private int posY;
	private BufferedImage image;
	private boolean isBlocking;
	
	public Block(int x, int y, boolean isBlocking) {
		posX = x;
		posY = y;
		image = Bitmap.ERROR;
		this.isBlocking = isBlocking;
	}
	
	public Block(BufferedImage spr, int x, int y) {
		posX = x;
		posY = y;
		image = spr;
	}
	
	public int getPosX() {
		return posX;
	}
	
	public int getPosY() {
		return posY;
	}
	
	public BufferedImage getImage() {
		return image;
	}
	
	public void setImage(BufferedImage img) {
		image = img;
	}
	
	public boolean isBlocking() {
		return isBlocking;
	}
	
	public void setPos(int x, int y) {
		posX = x;
		posY = y;
	}
	
	public void render(Graphics2D g, Screen scr) {
		int posX = getPosX();
		int posY = getPosY();
		int offset = scr.getOffset();
		if(posX + getImage().getWidth() < offset || posX > offset + scr.getWidth())
			return;
		g.drawImage(getImage(), posX - offset, posY, null);
	}
	
	abstract public void tick();
}
