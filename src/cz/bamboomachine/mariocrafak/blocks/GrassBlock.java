package cz.bamboomachine.mariocrafak.blocks;

import java.awt.image.BufferedImage;
import java.util.Random;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class GrassBlock extends Block {

	public GrassBlock(int x, int y) {
		super(x, y, true);
		Random rnd = new Random();
		BufferedImage img = null;
		switch(rnd.nextInt(4)) {
		case 0:
			img = Bitmap.GRASS1; break;
		case 1:
			img = Bitmap.GRASS2; break;
		case 2:
			img = Bitmap.GRASS3; break;
		case 3:
			img = Bitmap.GRASS4; break;
		}
		setImage(img);
	}

	@Override
	public void tick() {}

}
