package cz.bamboomachine.mariocrafak.blocks;

import cz.bamboomachine.mariocrafak.gfx.Bitmap;

public class BoxHearthBlock extends Block {

	public BoxHearthBlock(int x, int y) {
		super(x, y, true);
		setImage(Bitmap.HEARTHBOX);
	}

	@Override
	public void tick() {}

}
