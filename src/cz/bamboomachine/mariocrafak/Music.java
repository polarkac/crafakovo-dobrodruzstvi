package cz.bamboomachine.mariocrafak;

import java.io.BufferedInputStream;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Music {
	
	private static float volume = -15f;
	private static ArrayList<Music> musics = new ArrayList<Music>();
	
	public static Music MENU = new Music("Melodie_menu_v2.wav");
	public static Music SONG = new Music("song_1_copy.wav");
	
	private Clip clip;
	
	private Music(String name) {
		try {
			AudioInputStream stream = AudioSystem.getAudioInputStream(new BufferedInputStream(Music.class.getResourceAsStream("/sounds/" + name)));
			clip = AudioSystem.getClip();
			clip.open(stream);
			setVolume();
			
			musics.add(this);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void playLoop() {
		if(clip != null) {
			if(clip.isRunning())
				return;
			
			new Thread() {
				public void run() {
					synchronized(clip) {
						clip.loop(Clip.LOOP_CONTINUOUSLY);
					}
				}
			}.start();
		}
	}
	
	public void stop() {
		if(clip != null && clip.isRunning()) {
			clip.stop();
		}
	}
	
	public void setVolume() {
		FloatControl ctrl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		ctrl.setValue(volume);
	}
	
	public static void increaseVolume() {
		if(volume == 6)
			return;
		volume += 1f;
		for(Music s : musics) {
			s.setVolume();
		}
	}

	public static void decreaseVolume() {
		if(volume == -80)
			return;
		volume -= 1f;
		for(Music s : musics) {
			s.setVolume();
		}
	}
	
	public static float getVolume() {
		return volume;
	}
}
