package cz.bamboomachine.mariocrafak;

import java.io.BufferedInputStream;
import java.util.ArrayList;

import javax.sound.sampled.AudioInputStream;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import javax.sound.sampled.FloatControl;

public class Sound {
	
	private static float volume = -15f;
	private static ArrayList<Sound> sounds = new ArrayList<Sound>();
	
	public static Sound JUMP = new Sound("Jump 004.wav");
	public static Sound AHOJ = new Sound("crafak_ahoj.wav");	
	public static Sound BUTTON_LONG = new Sound("Long_ButtonMenu.wav");		
	public static Sound SHOR = new Sound("crafak_shor.wav");
	
	private Clip clip;
	
	/*
	 * Make classes for sounds (jump, button etc) and music
	 */	
	
	private Sound(String name) {
		try {
			AudioInputStream stream = AudioSystem.getAudioInputStream(new BufferedInputStream(Sound.class.getResourceAsStream("/sounds/" + name)));
			clip = AudioSystem.getClip();
			clip.open(stream);
			setVolume();
			
			sounds.add(this);
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public void play() {
		if(clip != null) {
			clip.stop();
			new Thread() {
				public void run() {
					clip.setFramePosition(0);
					clip.start();
				}
			}.start();
		}
	}
	
	public void stop() {
		if(clip != null && clip.isRunning())
			clip.stop();
	}
	
	public void setVolume() {
		FloatControl ctrl = (FloatControl) clip.getControl(FloatControl.Type.MASTER_GAIN);
		ctrl.setValue(Sound.volume);
	}
	
	public static void increaseVolume() {
		if(volume == 6)
			return;
		volume += 1f;
		for(Sound s : sounds) {
			s.setVolume();
		}
	}

	public static void decreaseVolume() {
		if(volume == -80)
			return;
		volume -= 1f;
		for(Sound s : sounds) {
			s.setVolume();
		}
	}
	
	public static float getVolume() {
		return volume;
	}
}
